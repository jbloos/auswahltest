from kaggle.api.kaggle_api_extended import KaggleApi
import os
import zipfile


#Funktionalität zum Downloaden des Datensatzes von Kaggle, falls er noch nicht vorhanden ist
if not os.path.exists("dataset"):
    api = KaggleApi()
    api.authenticate()

    api.dataset_download_files('patrickaudriaz/tobacco3482jpg')


    with zipfile.ZipFile("tobacco3482jpg.zip","r") as z:
        z.extractall("dataset/")
    