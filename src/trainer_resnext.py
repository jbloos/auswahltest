#prüfe ob code von konsole oder in jupyter notebook läuft und importiere andere files nur wenn es in konsole ist
from IPython import get_ipython  
from model import AlexNetClassifier, ResNextClassifier
from dataloader import Dataloader
from confusion_matrix import ConfusionMatrix
#importiere Abhängigkeiten
import matplotlib.pyplot as plt
import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
import pandas as pd
import seaborn as sn

def run():
    #groeße des Testsplits
    test_size = 20

    #erzeuge Dataloader Instanz
    dloader = Dataloader("dataset/tobacco3482-jpg/Tobacco3482-jpg")

    #Funktion um geladene Daten in Batches einzuteilen
    #dabei enthält jeder Batch wie im Paper 10 Datenpunkte. 
    #in diese Implementierung exakt einer pro Klasse
    def prepare_batches(data, categories):
        num_batches = len(data[categories[0]])
        batches = []
        for i in range(num_batches):
            batch = []
            for category in categories:
                batch.append(data[category][i].get_tensor())
            batches.append(torch.stack(batch))
        return batches

    #da es immer exakt ein Datenpunkt pro Klasse in einem Batch sind, kann man das target fix auf [1,2,3,...,10] setzen
    target = torch.arange(10).cuda()


    sample_size = 100
    with dloader:
        #das Netzwerk muss für jede Messung neu Initialisiert werden, um Overfit und ungewolltes Pretraining zu vermeiden
        net = ResNextClassifier().cuda()
        #lade test und train splits
        #dloader merkt sich welche Daten schon geladen wurden. test und train sind also disjunkt
        test_split = dloader.load(test_size, (227,227))
        train_split = dloader.load(sample_size, (227,227)) 

        #teile train split in batches ein
        batches = prepare_batches(train_split ,dloader.categories)

        #loss und optimizer fürs trainieren des Netzes
        criterion = nn.CrossEntropyLoss()
        optimizer = optim.SGD(net.parameters(), lr=0.0001, momentum=0.9)

        #setze Netzwerk in train Modus
        net.train()

        #trainiere für 40 Epochen
        for epoch in range(40):
            running_loss = 0

            #trainiere einen Batch
            for batch in batches:

                #Gib dem netzwerk die Information mit, ob es gerade die ersten 10 Epochen trainiert.
                #Ist das der Fall, sind die Gradienten für die Feature Extraction des Netzes deaktiviert.
                #Das friert die Feature Extraction beim Training ein. Das schützt die vortrainierten Parameter, bis 
                #der Klassifikator genug konvergiert ist
                output = net(batch, epoch > 20).cuda()
                loss = criterion(output.cuda(), target)
                loss.backward()
                optimizer.step()
                # print(loss)
                running_loss += loss.item()
                optimizer.zero_grad()
            #teile auch den test Datensatz in batches ein. Das ist eigentlich nicht nötig, weil sowieso alles in die Grafikkarte passt.
            #Es war auf diese Weise aber etwas leichter zu implementieren, weil alle Funktionen bereits für das Training implementiert wurden
            test_batches = prepare_batches(test_split, dloader.categories)
            #setze Netzwerk in evaluation Mode
            net.eval()
            #initialisiere Confusion Matrix
            confusion_matrix = ConfusionMatrix()
            #evaluiere alle testbatches, bestimme die Klasse und speichere das Ergebnis in confusion Matrix
            for batch in test_batches:
                pred = F.softmax(net(batch),dim=0).argmax(dim=0)
                # print(pred)
                pred = [dloader.categories[x.item()] for x in pred]
                confusion_matrix.add(dloader.categories, pred)
            
            #lese accuracy aus confusion matrix aus (=Summe der Diagonale)
            accuracy = confusion_matrix.get_accuracy(dloader.categories)

            #speichere accuracy und sample size, um es später als Graph anzuzeigen

            print("epoch " + str(epoch) +  " loss: " + str(running_loss) + "  accuracy: " + str(accuracy))


    #plotte die Ergebnisse und zeige sie an
    plt.figure()
    confusion_matrix.plot_matrix()
    plt.figure()
    confusion_matrix.plot_barplot(dloader.categories)
    plt.figure()
    plt.show()


if __name__ == "__main__":
    run()