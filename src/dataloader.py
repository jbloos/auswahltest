import os
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
import cv2
import torch
from random import shuffle

#kleine Helperklasse, welche ein Bild darstellt. 
#Sie hilft den Zugriff auf die Future Objekte zu kapseln
class Image:
    def __init__(self, future):
        self.future = future
        self.image = None

    def show(self):
        if self.image is None:
            self.image=self.future.result()
        cv2.imshow('',self.image.numpy()[0])
        cv2.waitKey(1000)

    def get_tensor(self):
        if self.image is None:
            self.image=self.future.result()
        return self.image


#funktion, welche ein Bild von einem Pfad (image) zu laden, die größe auf 227x227 anzupassen
#und in einen Torch tensor zu laden, um es dann auf die Grafikkarte zu laden
def load(image, scale):
    img = cv2.imread(image,0)
    if scale is not None:
        img = cv2.resize(img, scale, interpolation = cv2.INTER_AREA)
    img = torch.from_numpy(img)
    img = torch.stack([img,img,img])
    return img.float().cuda()/255


#dataloader Klasse, welche Daten asynchron lädt
class Dataloader:

    #initialisiere alle Daten und scanne die Ordnerhierarche, um
    #daraus die Kategorien abzuleiten
    def __init__(self, path):
        self.path = path
        self.categories = os.listdir(self.path)
        self.files = {}
        self.executor = ThreadPoolExecutor(max_workers = 8)
        self.count = 0
        self.smallest_category = 1e9
        for category in self.categories:
            images = os.listdir(self.path + "/" + category)
            images = list(filter(lambda x: x.find(".jpg") != -1, images))
            shuffle(images)
            self.files[category] = images
            self.smallest_category = min(self.smallest_category, len(images))

    #falls das dataloader Objekt gelöscht wird, stoppe auch den threadpoolexecutor, sonst
    #terminiert das Programm gegebenenfalls nicht
    def __del__(self):
        self.executor.shutdown(wait=True)

    #der dataloader soll auch als contextmanager funktionieren, um den Zustand (count) einfacher zurückzusetzen.
    def __enter__(self):
        self.count = 0
    
    def __exit__(self,type, value, traceback):
        pass
    
    
    #lade "nr" anzahl von Datenpunkten und skaliere sie auf die Größe "scale". Scale ist dabei ein Tupel.
    #Das laden ist dabei asynchron, diese Funktion hier blockt also nicht
    def load(self, nr, scale):
        loaded_images = {}
        for category in self.categories:
            result = []
            for i in range(nr):
                future = self.executor.submit(load, "".join((self.path, "/", category, "/", self.files[category][self.count+i] )) ,scale)
                image = Image(future)
                result.append(image)
            loaded_images[category] = result
        self.count += nr
        return loaded_images
            