
import torch
import torch.nn as nn
from torchvision import models

#Alexnet Classifier, aber tausche das letzte Layer des Netzwerks aus, um 
#einen Klassifikator für 10 Klassen zu erhalten
class AlexNetClassifier(nn.Module):
    def __init__(self):
        super(AlexNetClassifier, self).__init__()
        alex = models.alexnet(pretrained = True)
        self.feature_extraction = nn.Sequential(*list(alex.features.children()))
        self.classifier = nn.Sequential(*list(alex.classifier.children())[:-1])
        self.final_layer = nn.Linear(4096, 10)

    #forward funktion des Netzwerks.
    def forward(self, x, train_feature_extraction=False):
        N, C, H, W = x.shape
        #wenn gewuenscht deaktiviere die Gradienten für die feature Extraction
        #das ermöglicht es nur den classifier zu trainieren, ohne die 
        #vortrainierten Gewichte der Feature Extraction zu beschädigen
        if train_feature_extraction:
            x = self.feature_extraction(x)
        else:
            x = self.feature_extraction(x).detach()  
        x = self.classifier(x.view(N, -1))
        x = self.final_layer(x)
        return x


#das gleiche nochmal für ResNext. Siehe dazu den Bericht
class ResNextClassifier(nn.Module):
    def __init__(self):
        super(ResNextClassifier, self).__init__()
        resnext = models.resnext50_32x4d(pretrained = True)
        self.net = nn.Sequential(*list(resnext.children())[:-1])
        self.final_layer = nn.Linear(2048, 10)

    def forward(self, x, train_feature_extraction=False):
        N, C, H, W = x.shape
        if train_feature_extraction:
            x = self.net(x)
        else:
            x = self.net(x).detach()  
        x = self.final_layer(x.view(N, -1))
        return x