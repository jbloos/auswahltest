
from collections import defaultdict 
import pandas as pd
import seaborn as sn
import numpy as np
import matplotlib.pyplot as plt


#Klasse für die Confusion Matrix
#sie speichert die nötigen Informationen zum Berechnen der 
#accuracy und bietet außerdem die Möglichkeit die confusion Matrix grafisch anzuzeigen.
#Die Matrix speichert allerdings absolute Werte und (noch) keine Prozente. Die 
#werden erst beim Zugriff auf die Daten berechnet
class ConfusionMatrix():
    #initialisiere alle nötigen Datenstrukturen
    #Die Matrix selbst ist dabei ein dictionary, das dictionarys enthält.
    #Nutze dafür defaultdicts, um sich das Initialisieren zu sparen
    def __init__(self):
        self.matrix = defaultdict(lambda: defaultdict(lambda:0))
        self.classcount = defaultdict(lambda: 0)
        self.count = 0

    #implementiere __getitem__ um Matrix über den [] Operator nutzen zu können
    def __getitem__(self, key):
        return self.matrix[key]

    #addiere eine Liste von Ergebnissen zur Matrix hinzu
    def add(self,ground_truth_list,prediction_list):
        for ground_truth, prediction in zip(ground_truth_list, prediction_list):
            self[prediction][ground_truth] +=1
            self.count +=1
            self.classcount[ground_truth] +=1
    
    #accuracy ist die summe der Diagonale geteilt durch die Gesamtzahl der eingetragenen Messwerte
    def get_accuracy(self, all_labels):
        correct = 0
        for label in all_labels:
            correct += self[label][label]

        return correct / self.count

    #plotte die Matrix als Heatmap
    def plot_matrix(self):
        df = pd.DataFrame(self.matrix)
        df = df.fillna(0)
        df = df.reindex(sorted(df.columns), axis=1)
        df = df.sort_index()
        classcount_df = pd.DataFrame(self.classcount, index = [0])
        df=df.div(classcount_df.iloc[0])

        sn.heatmap(df, cmap = sn.color_palette("light:b", as_cmap=True), annot = True)
        # plt.show()

    #plotte die Accuracy pro Klasse als Barchart
    def plot_barplot(self, categories):
        data = {category:self.matrix[category][category]/self.classcount[category] for category in categories}
        df = pd.DataFrame(data, index = [0])
        df = df.reindex(sorted(df.columns), axis=1)
        sn.catplot(color="#004686", data=df, kind = "bar")

        # plt.show()
