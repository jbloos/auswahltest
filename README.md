# auswahltest

Zum Ausführen der Python Scripte entweder das jupyter notebook im Unterordner 'src' öffnen, oder in einer Konsole in diesen Unterordner wechseln und von dort die Scripte mit 
```python3 trainer_alexnet.py``` <br>
bzw <br>
```python3 trainer_resnext.py``` <br>
ausführen.

<br><br>
Den Bericht und dessen LaTeX Files finden Sie im Unterordner 'report'
